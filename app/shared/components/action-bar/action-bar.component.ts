import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Location} from "@angular/common";

@Component({
    moduleId: module.id,
    selector: "action-bar",
    templateUrl: "action-bar.component.html"
})
export class ActionBarComponent {
    @Input() title: string;
    @Input() showEditButton: boolean;
    @Output() action: EventEmitter<any> = new EventEmitter();
    constructor(private location: Location){
        console.log(this.title);
    }

    edit(){
        this.action.emit();
    }

    save(){
        this.action.emit();
    }

    goBack(){
        this.location.back();
    }
}