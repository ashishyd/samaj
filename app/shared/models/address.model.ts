import {IUser} from "./user.model";

export interface IAddress {
    addressId?: number;
    addressName?: string;
    buildingName?: string;
    addressLine?: string;
    street?: string;
    city?: string;
    district?: string;
    state?: string;
    country?: string;
    pinCode?: string;
    landmark?: string;
    centrixGroup?: string;
    centrixNumber?: number;
    createdBy?: number;
    modifiedBy?: number;
    createdAt?: string;
    modifiedAt?: string;
}
export class AddressModel{
    addressId: number;
    addressName: string;
    buildingName: string;
    addressLine: string;
    street: string;
    city: string;
    district: string;
    state: string;
    country: string;
    pinCode: string;
    landmark: string;
    centrixGroup: string;
    centrixNumber: number;
    createdBy: number;
    modifiedBy: number;
    createdAt: string;
    modifiedAt: string;

    constructor(model?: IUser){
        if(model){
            for(let key in model){
                this[key] = model[key];
            }
        }
    }
}