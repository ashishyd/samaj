import moment = require("moment");
import {AddressModel} from "./address.model";
import {CommunityModel} from "./community.model";

export interface IUser{
    memberId?: number;
    password?: string;
    title?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    mobile?: number;
    image?: any;
    dateOfBirth?: moment.Moment;
    placeOfBirth?: string;
    education?: string;
    occupation?: string;
    bloodGroup?: string;
    nativeAddress?: AddressModel;
    residenceAddress?: AddressModel;
    officeAddress?: AddressModel;
    relationshipType?: string;
    relationshipName?: string;
    relatedMember?: UserModel;
    maritalStatus?: string;
    inLawsGotra?: string;
    isHeadOfFamily?: boolean;
    parentId?: UserModel;
    dateOfExit?: moment.Moment;
    createdBy?: number;
    modifiedBy?: number;
    createdAt?: string;
    modifiedAt?: string;
}
export class UserModel{
    memberId: number;
    password: string;
    title: string;
    firstName: string;
    lastName: string;
    email: string;
    mobile: number;
    image: any;
    dateOfBirth: moment.Moment;
    placeOfBirth: string;
    education: string;
    occupation: string;
    bloodGroup: string;
    nativeAddress: any;
    residenceAddress: any;
    officeAddress: any;
    relationshipType: string;
    relationshipName: string;
    relatedMember: UserModel;
    maritalStatus: string;
    inLawsGotra: string;
    isHeadOfFamily: boolean;
    parentId: UserModel;
    dateOfExit: moment.Moment;
    createdBy: number;
    modifiedBy: number;
    createdAt: string;
    modifiedAt: string;
    communities: CommunityModel[] = [];

    constructor(model?: IUser){
        if(model){
            for(let key in model){
                this[key] = model[key];
            }
        }
    }

    get fullName(): string{
        return `${this.firstName} ${this.lastName}`;
    }

}