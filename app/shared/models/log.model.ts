import moment = require("moment");

export interface ILog{
    Id: number;
    Type: LogType;
    Message: string;
    LoggedDate: string;
}
export enum LogType{
    DEBUG = "DEBUG",
    INFO = "INFO",
    ERROR = "ERROR",
}
export class LogModel{
    public Id: number;
    public Type: LogType;
    public Message: string;
    public LoggedDate: string;
    //optional as we can use constructor as blank too
    constructor(private _message?: string, private _type?: LogType){
        this.Type = (this._type) ? this._type : LogType.ERROR;
        if(this._message) {
            this.Message = this._message;
            this.LoggedDate = moment().toLocaleString();
        }
    }
}