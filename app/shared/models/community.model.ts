import {IUser} from "./user.model";

export interface ICommunity {
    communityId?: number;
    communityName?: string;
    communityLogo?: string;
    isClosed?: boolean;
}
export class CommunityModel {
    communityId: number;
    communityName: string;
    communityLogo: string;
    isClosed: boolean;

    constructor(model?: ICommunity){
        if(model){
            for(let key in model){
                this[key] = model[key];
            }
        }
    }
}