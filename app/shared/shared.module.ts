import {NgModule} from "@angular/core";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";
import {NativeScriptFormsModule, NativeScriptHttpModule, NativeScriptRouterModule} from "nativescript-angular";
import {TNSFontIconModule} from "nativescript-ngx-fonticon";
import {ActionBarComponent} from "./components/action-bar/action-bar.component";
import {DropDownModule} from "nativescript-drop-down/angular";
const MODULES: any[] = [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptRouterModule,
    NativeScriptHttpModule,
    DropDownModule,
    TNSFontIconModule
];
@NgModule({
    imports: [...MODULES],
    declarations: [ActionBarComponent],
    exports: [...MODULES, ActionBarComponent]
})
export class SharedModule{}