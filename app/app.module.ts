import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {LoggedInLazyLoadGuard} from "./logged-in-lazy-load.guard";
import {CoreModule} from "./core/core.module";
import {TNSFontIconModule} from "nativescript-ngx-fonticon";
import {registerElement} from "nativescript-angular";
registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView);

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        CoreModule,
        AppRoutingModule,
        TNSFontIconModule.forRoot({
            'fa': './assets/font-awesome.css',
            'mdi': './assets/material-design-icons.css'
        })
    ],
    declarations: [
        AppComponent
    ],
    providers:[
        LoggedInLazyLoadGuard
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
