import {NgModule, Optional, SkipSelf} from "@angular/core";
import {PROVIDERS} from "./services";
import {
    ModalDialogService, NativeScriptFormsModule, NativeScriptHttpModule,
    NativeScriptRouterModule
} from "nativescript-angular";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";
import {DropDownModule} from "nativescript-drop-down/angular";
import {DIRECTIVES} from "./directives";
import {TNSTextToSpeech} from "nativescript-texttospeech";
import {SpeechRecognition} from "nativescript-speech-recognition";

const MODULES: any[] = [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptRouterModule,
    NativeScriptHttpModule,
    DropDownModule
];
const CORE_DIRECTIVES: any[] = [
    ...DIRECTIVES
];
const CORE_PROVIDERS: any[] = [
    ...PROVIDERS,
    ModalDialogService,
    TNSTextToSpeech,
    SpeechRecognition
];

@NgModule({
    imports: [...MODULES],
    declarations: [...CORE_DIRECTIVES],
    providers: [...CORE_PROVIDERS],
    exports: [...MODULES]
})
export class CoreModule {
    constructor(
        @Optional() @SkipSelf() parentModule: CoreModule
    ){
        if(parentModule){
            throw new Error('CoreModule already loaded. Import it in AppModule only');
        }
    }
}