import {IsEmailDirective, MinLengthDirective} from "./input.directive";
import {IfAndroidDirective, IfIosDirective} from "./if-platform.directive";

export const DIRECTIVES: any[] = [
    MinLengthDirective,
    IsEmailDirective,
    IfAndroidDirective,
    IfIosDirective
];