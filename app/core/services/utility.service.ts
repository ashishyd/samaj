import {Injectable} from "@angular/core";

let CryptoJS = require("crypto-js");
@Injectable()
export class UtilityService{
    private static readonly VERY_SECRET_CODE: string = 'My very secret code';
    public static actionBarTitle: string = "EvoExpense";
    public static encryptData(plaintText: string): string{
        // we use the AES-256 encryption -> very secure
        return CryptoJS.AES.encrypt(plaintText, this.VERY_SECRET_CODE);
    }
    public static decryptData(encryptedText: string): string {
        let bytes = CryptoJS.AES.decrypt(encryptedText, this.VERY_SECRET_CODE);
        return bytes.toString(CryptoJS.enc.Utf8);
    }
}