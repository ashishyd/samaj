import {Injectable} from "@angular/core";
import {DatabaseService} from "./database.service";
import {CommunityModel} from "../../shared/models/community.model";

@Injectable()
export class CommunityService {
    private communities: CommunityModel[] = [];
    constructor(private databaseService: DatabaseService){}

    init(){
        this.communities = [];
        this.databaseService.all("SELECT * FROM COMMUNITY", [])
            .then(rows => {
                rows.forEach(row => {
                    this.communities.push(new CommunityModel({
                        communityId: row[0],
                        communityName: row[1],
                        communityLogo: row[2],
                        isClosed: row[3] === 1
                    }));
                });
            });
    }

    getAllCommunities(): CommunityModel[]{
        return this.communities;
    }

    getCommunityById(id: number): CommunityModel{
        return this.communities.find(x => x.communityId === id);
    }
}