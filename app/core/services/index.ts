import {Constants} from "./constant.service";
import {DatabaseService} from "./database.service";
import {LoggingService} from "./logging.service";
import {UserService} from "./user.service";
import {UtilityService} from "./utility.service";
import {CommunityService} from "./community.service";

export const PROVIDERS: any[] = [
    Constants,
    DatabaseService,
    LoggingService,
    UserService,
    UtilityService,
    CommunityService
];
export * from './logging.service';
export * from './user.service';
export * from './utility.service';
export * from './community.service';
export * from './database.service';
