export class LoggingService{
    public static error(message: string){
        console.error(message);
    }
    public static debug(message: string){
        console.debug(message);
    }
    public static info(message: string){
        console.info(message);
    }
    public static warning(message: string){
        console.warn(message);
    }
}