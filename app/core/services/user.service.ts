import {Injectable} from "@angular/core";
import {DatabaseService} from "./database.service";
import {LoggingService} from "./logging.service";
import moment = require("moment");
import {UtilityService} from "./utility.service";
import {LogModel, LogType, UserModel, CommunityModel} from "../../shared/models";
import {Http} from "@angular/http";
import {AddressModel} from "../../shared/models/address.model";
import {CommunityService} from "./community.service";

@Injectable()
export class UserService {
    private users: UserModel[] = [];

    constructor(private databaseService: DatabaseService, private communityService: CommunityService) {
    }

    public init() {
        this.users = [];
        this.databaseService.all("SELECT * FROM MEMBERS", [])
            .then(rows => {
                rows.forEach(row => {
                    this.users.push(new UserModel({
                        memberId: row[0],
                        email: row[1],
                        password: row[2],
                        title: row[3],
                        firstName: row[4],
                        lastName: row[5],
                        mobile: row[6],
                        dateOfBirth: moment(row[7]),
                        placeOfBirth: row[8],
                        education: row[9],
                        occupation: row[10],
                        bloodGroup: row[11],
                        nativeAddress: row[12],
                        residenceAddress: row[13],
                        officeAddress: row[14],
                        relationshipType: row[15],
                        relationshipName: row[16],
                        relatedMember: row[17],
                        maritalStatus: row[18],
                        inLawsGotra: row[19],
                        isHeadOfFamily: row[20] === 1,
                        parentId: row[21],
                        dateOfExit: moment(row[22]),
                        createdBy: row[23],
                        modifiedBy: row[24],
                        createdAt: row[25],
                        modifiedAt: row[26]
                    }));
                });
            });
    }

    checkUserCredentials(user: UserModel): boolean {
        this.users.forEach(x => {
            console.log("database password: ", UtilityService.decryptData(x.password));
        });
        console.log("password", user.password);
        let userModel = this.users.find(x => (x.email.toLowerCase() == user.email.toLowerCase()) && UtilityService.decryptData(x.password) == user.password);
        return !!userModel;
    }

    public updateProfileImage(user: UserModel) {
        this.databaseService.exec("UPDATE MEMBERS SET IMAGE = ?, MODIFIED_AT =? WHERE Id=? ", [user.image, moment().format('LLL'), user.memberId]);
    }

    validateMobile(mobileNumber: number): boolean {
        return this.users.some(x => x.mobile === mobileNumber);
    }

    validateEmail(email: string): boolean {
        return this.users.some(x => x.email === email);
    }

    registerUser(user: UserModel): Promise<boolean> {
        let pass = UtilityService.encryptData(user.password);
        return this.databaseService.exec("INSERT INTO MEMBERS (PASSWORD, TITLE, FIRST_NAME, LAST_NAME, EMAIL, MOBILE, Created_AT) VALUES (?, ?, ?, ?, ?, ?, ?)",
            [pass, user.title, user.firstName, user.lastName, user.email, user.mobile, moment().format('LLL')])
            .then(id => {
                this.init();
                return !!id;
            }).catch(err => {
                LoggingService.error(`Failed to register user: ${err}`);
                return false;
            });
    }

    getUserFromId(id: number): UserModel {
        return this.users.find(x => x.memberId === id);
    }

    getUserFromUsername(username: string): UserModel {
        return this.users.find(x => x.email.toLowerCase() === username.toLowerCase());
    }

    resetPassword(email) {
        // return this.http.post(
        //     //BackendService.baseUrl + "rpc/" + BackendService.appKey + "/" + email + "/user-password-reset-initiate",
        //     // {},
        //     // { headers: this.getCommonHeaders() }
        //     console.log(email);
        // );
        return true;
    }

    getAllUserCommunities(member: UserModel){
        this.databaseService.all("SELECT * FROM COMMUNITY_MEMBERS WHERE MEMBER_ID", [member.memberId])
            .then(rows => {
                rows.forEach(row => {
                    member.communities.push(this.communityService.getCommunityById(row[0]));
                })
            });
    }
}
