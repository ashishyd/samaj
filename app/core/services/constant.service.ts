import {Injectable} from "@angular/core";

@Injectable()
export class Constants{
    public static DEFAULT_PROFILE_IMAGE_NAME: string = "profile.png";
    public static DEFAULT_PROFILE_IMAGE_PATH: string = "res://default_profile";
}