import {Injectable} from "@angular/core";
import {LoggingService} from "./logging.service";

let Sqlite = require("nativescript-sqlite");
@Injectable()
export class DatabaseService{
    public database: any;

    constructor(){}

    public initDatabase(): Promise<boolean>{
        /*it takes to copy a pre-filled database from the project’s app directory into the appropriate Android and iOS working spaces.
         */
        if(!Sqlite.exists("samaj.db")){
            Sqlite.copyDatabase("samaj.db");
        }
       return (new Sqlite("samaj.db")).then(db => {
            this.database = db;
            return true;
        }).catch((err: any) => {
            console.log("Error opening db: ", err);
            return false;
        });
    }

    public select(sqlStatement: string, params: any[]): Promise<any>{
        return this.database.get(sqlStatement, params);
    }

    public all(sqlStatement: string, params: any[]): Promise<any[]>{
        return this.database.all(sqlStatement, params);
    }

    public exec(sqlStatement: string, params: any[]): Promise<any>{
        return this.database.execSQL(sqlStatement, params).then(id => {
            return id;
        }).catch(err => {
            LoggingService.error(`Error executing sql statement: ${err}`);
            return null;
        })
    }
}