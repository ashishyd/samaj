import {Component, OnInit} from "@angular/core";
import {DatabaseService} from "./core/services";
import statusBar = require("nativescript-status-bar");
import {TNSFontIconService} from "nativescript-ngx-fonticon";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit{
    constructor(private databaseService: DatabaseService, private fontIconService: TNSFontIconService){

    }
    ngOnInit(){
        // firebase.init(
        //     {
        //         persist: false
        //     })
        //     .then(() => console.log(">>>>> Firebase initialized"))
        //     .catch(err => console.log(">>>>> Firebase init error: " + err));
        //hide the status bar for app
        statusBar.hide();
        //init database sqlite
        this.databaseService.initDatabase();
    }
}
