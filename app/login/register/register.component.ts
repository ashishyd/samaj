import {Component, NgZone, OnInit} from "@angular/core";
import {Location} from "@angular/common";
import {Page} from "tns-core-modules/ui/page";
import {UserModel} from "../../shared/models";
import {Feedback, FeedbackType} from "nativescript-feedback";
import {LoggingService, UserService} from "../../core/services";
import {RouterExtensions} from "nativescript-angular/router";

@Component({
    moduleId: module.id,
    selector: "register",
    templateUrl: "register.component.html"
})
export class RegisterComponent implements OnInit{
    isLoading: boolean;
    private feedback: Feedback;
    public userModel: UserModel;

    constructor(private location: Location,
                private routerExtension: RouterExtensions,
                private userService: UserService,
                private ngZone: NgZone,
                private _page: Page){
    }

    ngOnInit(){
        this.isLoading = false;
        this.feedback = new Feedback();
        //this._page.actionBarHidden = true;
        this.userModel = new UserModel();
    }

    onRegisterButtonTap(){
        let error: boolean = false;
        if(this.userModel.email && this.userService.validateEmail(this.userModel.email)){
            this.feedback.show({
                message: `${this.userModel.email} already exists. Please sign-in.`,
                type: FeedbackType.Error
            });
            error = true;
        }
        else if(this.userModel.mobile && this.userService.validateMobile(this.userModel.mobile)){
            this.feedback.show({
                message: `${this.userModel.mobile} already exists. Please sign-in.`,
                type: FeedbackType.Error
            });
            error = true;
        }
        if(!error) {
            if (this.userModel.title
                && this.userModel.firstName
                && this.userModel.lastName
                && this.userModel.password
                && this.userModel.email
                && this.userModel.mobile) {
                this.userService.registerUser(this.userModel).then((data: boolean) => {
                    if (data) {
                        this.feedback.show({
                            message: `${this.userModel.email} registered successfully`,
                            type: FeedbackType.Success
                        });
                        this.location.back();
                    } else {
                        this.feedback.show({
                            message: `Could not register ${this.userModel.email}. Please try again.`,
                            type: FeedbackType.Error
                        });
                    }
                }).catch(err => {
                    this.feedback.show({
                        message: `Could not register ${this.userModel.email}. Please try again.`,
                        type: FeedbackType.Error
                    });
                    LoggingService.error(`Register user error ${err}`);
                })
            } else {
                this.feedback.show({
                    message: "All fields are required",
                    type: FeedbackType.Error
                });
            }
        }
    }
    onBackTap(){
        this.routerExtension.navigate(["/login"], {clearHistory: true});
        //this.location.back();
    }
}