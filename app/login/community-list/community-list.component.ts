import {Component, OnInit} from "@angular/core";
import {CommunityService} from "../../core/services/community.service";
import {CommunityModel} from "../../shared/models";
import {TNSFontIconService} from "nativescript-ngx-fonticon";

@Component({
    moduleId: module.id,
    selector: "community-list",
    templateUrl: "community-list.component.html"
})
export class CommunityListComponent implements OnInit{
    public groups: CommunityModel[];
    constructor(private communityService: CommunityService, private fonticon: TNSFontIconService){}

    ngOnInit(){
        this.groups = this.communityService.getAllCommunities();
    }
}