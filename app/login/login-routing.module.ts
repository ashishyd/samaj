import {Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {NativeScriptRouterModule} from "nativescript-angular";
import {LoginComponent} from "./login.component";
import {RegisterComponent} from "./register/register.component";
import {CommunityListComponent} from "./community-list/community-list.component";

const routes: Routes = [
    {path: "", component: LoginComponent},
    {path: "registration", component: RegisterComponent},
    {path: "community", component: CommunityListComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class LoginRouteModule{}