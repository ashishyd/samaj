import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {RouterExtensions} from "nativescript-angular/router";
import * as ApplicationSettings from "application-settings";
import {UserModel} from "../shared/models";
import {Page} from "tns-core-modules/ui/page";
import {Feedback, FeedbackType, FeedbackPosition} from "nativescript-feedback";
import {UserService} from "../core/services";
import {prompt} from "tns-core-modules/ui/dialogs";
import {TextField} from "tns-core-modules/ui/text-field";

@Component({
    moduleId: module.id,
    selector: "login",
    templateUrl: "login.component.html"
})
export class LoginComponent implements OnInit {
    isLoading: boolean;
    private feedback: Feedback;
    public userModel: UserModel;

    public constructor(private _routerExtension: RouterExtensions,
                       private _page: Page,
                       private userService: UserService) {
    }

    ngOnInit() {
        this.isLoading = false;
        this.userModel = new UserModel();
        this.feedback = new Feedback();
        this._page.actionBarHidden = true;
        if (ApplicationSettings.getString("username")) {
            this.userModel.email = ApplicationSettings.getString("username");
        }
    }

    onLoginButtonTap() {
        if (this.userModel.email && this.userModel.password) {
            if (this.userService.checkUserCredentials(this.userModel)) {
                if (!ApplicationSettings.getString("username")) {
                    ApplicationSettings.setString("username", this.userModel.email);
                }
                ApplicationSettings.setBoolean("authenticated", true);
                this._routerExtension.navigate(["/login/community"], {
                    clearHistory: true,
                    animated: true,
                    transition: {
                        name: "slide",
                        duration: 200,
                        curve: "ease"
                    }
                });
               // this._routerExtension.navigate(["/tabs"], {clearHistory: true});
            } else {
                this.feedback.show({
                    message: "Incorrect Credentials!",
                    type: FeedbackType.Error
                });
            }
        } else {
            this.feedback.show({
                message: "All fields are required",
                type: FeedbackType.Error
            });
        }
    }

    onRegisterButtonNavigateTap() {
        this._routerExtension.navigate(["/login/registration"], {
            animated: true,
            transition: {
                name: "slide",
                duration: 200,
                curve: "ease"
            }
        })
    }
    forgotPassword() {
        prompt({
            title: "Forgot Password",
            message: "Enter the email address you used to register for Samaj to reset your password.",
            defaultText: "",
            okButtonText: "Ok",
            cancelButtonText: "Cancel"
        }).then((data) => {
            if (data.result) {
                if(this.userService.resetPassword(data.text.trim())){
                    alert("Your password was successfully reset. Please check your email for instructions on choosing a new password.");
                }
                    // .subscribe(() => {
                    //     alert("Your password was successfully reset. Please check your email for instructions on choosing a new password.");
                    // }, () => {
                    //     alert("Unfortunately, an error occurred resetting your password.");
                    // });
            }
        });
    }
}