import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {LoginComponent} from "./login.component";
import {LoginRouteModule} from "./login-routing.module";
import {RegisterComponent} from "./register/register.component";
import {NativeScriptFormsModule} from "nativescript-angular";
import {SharedModule} from "../shared/shared.module";
import {CommunityListComponent} from "./community-list/community-list.component";

@NgModule({
    imports: [
        SharedModule,
        LoginRouteModule
    ],
    declarations: [
        LoginComponent,
        RegisterComponent,
        CommunityListComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LoginModule{}