import {CanLoad} from "@angular/router";
import {RouterExtensions} from "nativescript-angular";
import {Injectable} from "@angular/core";
import {CommunityService, UserService} from "./core/services";
import * as ApplicationSettings from "tns-core-modules/application-settings";

@Injectable()
export class LoggedInLazyLoadGuard implements CanLoad{
    constructor(private _routerExtensions: RouterExtensions, private communityService: CommunityService, private userService: UserService){}

    canLoad(): boolean{
        //ApplicationSettings.setString("username", "");
        if (!ApplicationSettings.getString("username")) {
            this._routerExtensions.navigate(["login"], { clearHistory: true });
        }
        //initialize the Country
        this.communityService.init();
        //initialize the User
        this.userService.init();
        return true;
    }
}